package com.epam.menu.impl;

import com.epam.model.SuperObject;
import com.epam.menu.MenuOption;
import com.epam.service.menu.AvroSerializeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class AvroSerializeMenuOption implements MenuOption {

    private static final Logger LOGGER = LoggerFactory.getLogger(AvroSerializeMenuOption.class);

    @Autowired
    private AvroSerializeService avroSerializer;

    @Override
    public void execute(SuperObject superObject) {
        try {
            avroSerializer.serialize(superObject.getAddress(), superObject.getPojoRules());
            System.out.println("Rules serialization finished successfully.");
        } catch (IllegalArgumentException e){
            LOGGER.error("List with pojo rules is empty, you should initialize it first", e);
        } catch (IOException e) {
            LOGGER.error("Error ocured while serializing pojo rules", e);
        }
    }

    public AvroSerializeService getAvroSerializer() {
        return avroSerializer;
    }

    public void setAvroSerializer(AvroSerializeService avroSerializer) {
        this.avroSerializer = avroSerializer;
    }
}
