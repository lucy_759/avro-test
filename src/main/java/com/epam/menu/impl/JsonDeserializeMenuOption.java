package com.epam.menu.impl;

import com.epam.model.RulePOJO;
import com.epam.model.SuperObject;
import com.epam.menu.MenuOption;
import com.epam.service.menu.JsonDeserializeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class JsonDeserializeMenuOption implements MenuOption {

    private static Logger LOGGER = LoggerFactory.getLogger(JsonDeserializeMenuOption.class);

    @Autowired
    private JsonDeserializeService jsonDeserializeService;

    @Override
    public void execute(SuperObject superObject) {
        String path = superObject.getAddress();
        try {
            List<RulePOJO> deserPojos = jsonDeserializeService.deserialize(path);
            if (!deserPojos.isEmpty()) {
                System.out.println(String.join(" ", "Rules read from ", path, "file."));
                deserPojos.forEach(System.out::println);
            }
            superObject.setPojoRules(deserPojos);
        } catch (IOException e) {
            LOGGER.error("An exception occured while deserializing pojos from json file.", e);
        }
    }

    public JsonDeserializeService getJsonDeserializeService() {
        return jsonDeserializeService;
    }

    public void setJsonDeserializeService(JsonDeserializeService jsonDeserializeService) {
        this.jsonDeserializeService = jsonDeserializeService;
    }
}
