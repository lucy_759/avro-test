package com.epam.menu.impl;

import com.epam.model.RulePOJO;
import com.epam.model.SuperObject;
import com.epam.model.avro.Rule;
import com.epam.menu.MenuOption;
import com.epam.service.menu.AvroDeserializeService;
import com.epam.utill.RulesTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AvroDeserializeMenuOption implements MenuOption {

    private static final Logger LOGGER = LoggerFactory.getLogger(AvroDeserializeMenuOption.class);

    @Autowired
    private AvroDeserializeService avroDeserializeService;

    @Override
    public void execute(SuperObject superObject) {
        String path = superObject.getAddress();
        try {
            List<Rule> deserializedRules = avroDeserializeService.deserialize(path);
            System.out.println("Deserialized rules:");
            deserializedRules.forEach(System.out::println);
            superObject.setRules(deserializedRules);
            List<RulePOJO> pojoList = deserializedRules.stream()
                    .map(RulesTransformer::transformFromRuleToPOJO)
                    .collect(Collectors.toList());
            superObject.setPojoRules(pojoList);
        } catch (IOException e) {
            LOGGER.error("An error occurred while deserializeing file", e);
        }
    }

    public AvroDeserializeService getAvroDeserializeService() {
        return avroDeserializeService;
    }

    public void setAvroDeserializeService(AvroDeserializeService avroDeserializeService) {
        this.avroDeserializeService = avroDeserializeService;
    }
}
