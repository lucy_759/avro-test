package com.epam.menu;

import com.epam.model.SuperObject;

/**
 * Created by Marko_Tsura on 7/5/2017.
 */
public interface MenuOption {
    void execute(SuperObject superObject);
}
