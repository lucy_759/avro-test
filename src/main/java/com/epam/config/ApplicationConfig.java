package com.epam.config;

import avro.shaded.com.google.common.collect.ImmutableMap;
import com.epam.menu.MenuOption;
import com.epam.menu.impl.AvroDeserializeMenuOption;
import com.epam.menu.impl.AvroSerializeMenuOption;
import com.epam.menu.impl.JsonDeserializeMenuOption;
import com.epam.model.SuperObject;
import com.epam.model.avro.Rule;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.Map;
import java.util.Scanner;


@Configuration
@ComponentScan(value = "com.epam")
public class ApplicationConfig {
    @Bean
    public DatumReader<Rule> datumReader() {
        return new SpecificDatumReader<>();
    }

    @Bean
    public DatumWriter<Rule> datumWriter() {
        return new SpecificDatumWriter<>();
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public SuperObject superObject() {
        return new SuperObject();
    }

    @Bean
    public Scanner scanner() {
        return new Scanner(System.in);
    }

    @Bean
    public Map<String, MenuOption> options(JsonDeserializeMenuOption jsonDeserializeMenuOption, AvroSerializeMenuOption avroSerializeMenuOption, AvroDeserializeMenuOption avroDeserializeMenuOption) {
        return ImmutableMap.of("json", jsonDeserializeMenuOption, "serialize", avroSerializeMenuOption, "deserialize", avroDeserializeMenuOption);
    }
}
