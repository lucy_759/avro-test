package com.epam.utill;

import avro.shaded.com.google.common.collect.ImmutableBiMap;
import com.epam.model.EntityState;
import com.epam.model.RulePOJO;
import com.epam.model.avro.Dsl;
import com.epam.model.avro.Rule;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class RulesTransformer {

    public static Rule transformPOJOToRule(RulePOJO pojo) {
        Set<String> flows = pojo.getFlows();
        List<CharSequence> collect = flows.stream().map(cs -> (CharSequence) cs).collect(Collectors.toList());
        return Rule.newBuilder()
                .setId(pojo.getId())
                .setVersion(Double.parseDouble(pojo.getVersion()))
                .setName(pojo.getName())
                .setDescription(pojo.getDescription())
                .setCreated(pojo.getCreated().getTime())
                .setUpdated(pojo.getUpdated().getTime())
                .setState(pojo.getState().name())
                .setEnabled(pojo.getEnabled())
                .setAttributes(new Dsl((CharSequence) pojo.getAttributes().get("dsl")))
                .setFlows(collect)
                .build();
    }

    public static RulePOJO transformFromRuleToPOJO(Rule newRule) {
        return RulePOJO.newBuilder()
                .withId(newRule.getId().toString())
                .withVersion(newRule.getVersion().toString())
                .withName(newRule.getName().toString())
                .withDescription(newRule.getDescription().toString())
                .withCreated(new Date(newRule.getCreated()))
                .withUpdated(new Date(newRule.getUpdated()))
                .withState(EntityState.valueOf(newRule.getState().toString()))
                .withEnabled(newRule.getEnabled())
                .withAttributes(ImmutableBiMap.of("dsl", newRule.getAttributes().getDsl()))
                .withFlows(newRule.getFlows().stream().map(CharSequence::toString).collect(Collectors.toSet()))
                .build();
    }
}
