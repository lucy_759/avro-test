package com.epam;

import com.epam.config.ApplicationConfig;
import com.epam.menu.MenuOption;
import com.epam.model.SuperObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class App {

    @Autowired
    @Qualifier(value = "options")
    private Map<String, MenuOption> options;

    @Autowired
    private SuperObject superObject;

    @Autowired
    private Scanner scanner;

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        App app = context.getBean(App.class);
        app.start();
    }

    private void start() {
        showMessage();
        while (true) {
            String[] split = scanner.nextLine().split(" ");
            MenuOption option = options.get(split[0]);
            superObject.setAddress(split[1]);
            option.execute(superObject);
        }
    }

    private static void showMessage() {
        System.out.println("Avro Test v1.0");
        System.out.println("Commands:");
        System.out.println("1. \'json <filepath>\' - unmarshal rules from json file to collection of pojos and print them.");
        System.out.println("2. \'serialize <filepath>\' - serialize rules from collection of pojos avro file.");
        System.out.println("3. \'deserialize <filepath>\' - deserialize from avro file to rules and print them.");
        System.out.println("Type your command:");
    }

    public Map<String, MenuOption> getOptions() {
        return options;
    }

    public void setOptions(Map<String, MenuOption> options) {
        this.options = options;
    }

    public SuperObject getSuperObject() {
        return superObject;
    }

    public void setSuperObject(SuperObject superObject) {
        this.superObject = superObject;
    }
}
