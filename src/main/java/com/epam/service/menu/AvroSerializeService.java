package com.epam.service.menu;

import com.epam.model.RulePOJO;
import com.epam.model.avro.Rule;
import com.epam.utill.RulesTransformer;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Marko_Tsura on 7/5/2017.
 */
@Service
public class AvroSerializeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AvroSerializeService.class);

    @Autowired
    private DatumWriter<Rule> writer;

    public void serialize(String address, List<RulePOJO> pojoRules) throws IllegalArgumentException, IOException {
        File destFile = new File(address);
        if (pojoRules != null && !pojoRules.isEmpty()) {
            try (DataFileWriter<Rule> dataFileWriter = new DataFileWriter<>(writer)) {
                dataFileWriter.create(Rule.getClassSchema(), destFile);
                pojoRules.forEach(e -> appendRuleToWriter(dataFileWriter, e));
            }
        } else {
            throw new IllegalArgumentException("pojoRules is not initialized");
        }
    }

    private void appendRuleToWriter(DataFileWriter<Rule> writer, RulePOJO pojo) {
        try {
            writer.append(RulesTransformer.transformPOJOToRule(pojo));
        } catch (IOException e) {
            LOGGER.error("Error while serializing rules.", e);
        }
    }

    public DatumWriter<Rule> getWriter() {
        return writer;
    }

    public void setWriter(DatumWriter<Rule> writer) {
        this.writer = writer;
    }
}
