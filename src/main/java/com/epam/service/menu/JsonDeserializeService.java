package com.epam.service.menu;

import com.epam.model.RulePOJO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marko_Tsura on 7/6/2017.
 */
@Service
public class JsonDeserializeService {

    @Autowired
    private ObjectMapper mapper;

    public List<RulePOJO> deserialize(String path) throws IOException {
        List<RulePOJO> rules = new ArrayList<>();
        File file = new File(path);
        rules.addAll(mapper.readValue(file, mapper.getTypeFactory().constructCollectionType(List.class, RulePOJO.class)));
        return rules;
    }
}
