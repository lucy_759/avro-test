package com.epam.service.menu;

import com.epam.model.avro.Rule;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.io.DatumReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Service
public class AvroDeserializeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AvroDeserializeService.class);

    @Autowired
    private DatumReader<Rule> datumReader;

    public List<Rule> deserialize(String path) throws IOException {
        List<Rule> rules = new ArrayList<>();
        try (DataFileReader<Rule> dataFileReader = new DataFileReader<>(new File(path), datumReader)) {
            dataFileReader.forEach(rules::add);
        }
        return rules;
    }

    public DatumReader<Rule> getDatumReader() {
        return datumReader;
    }

    public void setDatumReader(DatumReader<Rule> datumReader) {
        this.datumReader = datumReader;
    }
}
