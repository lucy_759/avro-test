package com.epam.model;

import com.epam.model.avro.Rule;

import java.util.List;

/**
 * Created by Marko_Tsura on 7/5/2017.
 */
public class SuperObject {
    private List<Rule> rules;
    private List<RulePOJO> pojoRules;
    private String address;

    public SuperObject() {
    }

    public SuperObject(List<Rule> rules, List<RulePOJO> pojoRules, String address) {
        this.rules = rules;
        this.pojoRules = pojoRules;
        this.address = address;
    }

    public List<Rule> getRules() {
        return rules;
    }

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }

    public List<RulePOJO> getPojoRules() {
        return pojoRules;
    }

    public void setPojoRules(List<RulePOJO> pojoRules) {
        this.pojoRules = pojoRules;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
