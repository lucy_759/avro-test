package com.epam.model;

import com.epam.model.avro.Rule;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.*;

@JsonDeserialize(builder = RulePOJO.Builder.class)
public class RulePOJO {
    private String id;
    private String version;
    private String name;
    private String description;
    private Date created;
    private Date updated;
    private EntityState state;
    private Boolean enabled;
    private Map<String, Object> attributes = new HashMap<>();
    private Set<String> flows = Collections.emptySet();

    private RulePOJO() {
    }

    private RulePOJO(String id, String version, String name, String description, Date created, Date updated, EntityState state, Boolean enabled, Map<String, Object> attributes, Set<String> flows) {
        this.id = id;
        this.version = version;
        this.name = name;
        this.description = description;
        this.created = created;
        this.updated = updated;
        this.state = state;
        this.enabled = enabled;
        this.attributes = attributes;
        this.flows = flows;
    }

    private RulePOJO(Builder builder) {
        id = builder.id;
        version = builder.version;
        name = builder.name;
        description = builder.description;
        created = builder.created;
        updated = builder.updated;
        state = builder.state;
        enabled = builder.enabled;
        attributes = builder.attributes;
        flows = builder.flows;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getVersion() {
        return version;
    }


    public String getName() {
        return name;
    }


    public String getDescription() {
        return description;
    }


    public Date getCreated() {
        return Objects.isNull(this.created) ? null : new Date(this.created.getTime());
    }


    public Date getUpdated() {
        return Objects.isNull(this.updated) ? null : new Date(updated.getTime());
    }


    public Set<String> getFlows() {
        return flows;
    }


    public EntityState getState() {
        return state;
    }


    public Boolean getEnabled() {
        return enabled;
    }


    public Map<String, Object> getAttributes() {
        return attributes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        RulePOJO entity = (RulePOJO) o;

        return new EqualsBuilder().append(id, entity.id).append(version, entity.version)
                .append(name, entity.name).append(description, entity.description)
                .append(created, entity.created).append(updated, entity.updated)
                .append(state, entity.state).append(enabled, entity.enabled)
                .append(attributes, entity.attributes).append(flows, entity.flows).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(version).append(name).append(description)
                .append(created).append(updated).append(state).append(enabled).append(attributes)
                .append(flows).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", id)
                .append("version", version)
                .append("name", name)
                .append("description", description)
                .append("created", created)
                .append("updated", updated)
                .append("state", state)
                .append("enabled", enabled)
                .append("attributes", attributes)
                .append("flows", flows)
                .toString();
    }

    @JsonPOJOBuilder
    public static final class Builder {
        private String id;
        private String version;
        private String name;
        private String description;
        @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
        private Date created;
        @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
        private Date updated;
        private EntityState state;
        private Boolean enabled;
        private Map<String, Object> attributes;
        private Set<String> flows;

        private Builder() {
        }

        public Builder withId(String val) {
            id = val;
            return this;
        }

        public Builder withVersion(String val) {
            version = val;
            return this;
        }

        public Builder withName(String val) {
            name = val;
            return this;
        }

        public Builder withDescription(String val) {
            description = val;
            return this;
        }

        public Builder withCreated(Date val) {
            created = val;
            return this;
        }

        public Builder withUpdated(Date val) {
            updated = val;
            return this;
        }

        public Builder withState(EntityState val) {
            state = val;
            return this;
        }

        public Builder withEnabled(Boolean val) {
            enabled = val;
            return this;
        }

        public Builder withAttributes(Map<String, Object> val) {
            attributes = val;
            return this;
        }

        public Builder withFlows(Set<String> val) {
            flows = val;
            return this;
        }

        public RulePOJO build() {
            return new RulePOJO(this);
        }
    }
}
