package com.epam.model;

/**
 * Created by Marko_Tsura on 6/29/2017.
 */
public enum EntityState {
    ARCHIVED, DRAFT, LIVE
}